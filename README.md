# LOGREG_ievtau_migsta

Ieva Tauraite, Migle Staskunaite

Priskirtas duomenų rinkinys - 00005

Projekto žingsniai
Kodo paleidimas

1. Atliekamas suskaidymas pagal šalis bei valandas
2. Paleidžiamas prognozavimo bei metrikų sudarymo kodas (optimizuotas)
Prognozavimo bei metrikų sudarymui nepavyko suautomatizuoti taip, kad kodas skaičiuotų iškart abiems laiko pjūviams 000 ir 0001, 
todėl k reiškmę esančią kode reikia pakeisti į 0 arba 1. Prieš paleidžaint kodą komandinėje eilutėje reikia nudorydi kelią, 
kuriame yra duomenys, šalį (ID) bei abi koeficientų reikšmes (L1 ir L2).

"python3 logistine.py /home/vagrant/synced_dir/00005 --id 710 --l1 0.00001 --l2 0.0001"

3. Naudojant Jupyter Notebook, sujungiamos metrikos bei nubraižomi metrikų grafikai